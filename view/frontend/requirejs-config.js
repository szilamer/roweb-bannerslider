var config = {
    map: {
        '*': {
            'owlcarousel': "Roweb_BannerSlider/js/owlcarousel.min"
        }
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};