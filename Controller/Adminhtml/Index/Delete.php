<?php

namespace Roweb\BannerSlider\Controller\Adminhtml\Index;

/**
 * Class Delete
 * @package Roweb\BannerSlider\Controller\Adminhtml\Index
 */
class Delete extends \Magento\Backend\App\Action
{

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Roweb_BannerSlider::banner_slider');
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                $model = $this->_objectManager->create('Roweb\BannerSlider\Model\Banners');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                $this->messageManager->addSuccess(__('The banner has been deleted.'));
                $this->_eventManager->dispatch(
                    'adminhtml_banners_on_delete',
                    ['title' => $title, 'status' => 'success']
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_banners_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a banner to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
