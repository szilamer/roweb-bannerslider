<?php

namespace Roweb\BannerSlider\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Roweb\BannerSlider\Model\Banners as ModelBanners;

/**
 * Class InlineEdit
 * @package Roweb\BannerSlider\Controller\Adminhtml\Index
 */
class InlineEdit extends \Magento\Backend\App\Action
{
    /** @var PostDataProcessor */
    protected $dataProcessor;
    /**
     * @var ModelBanners
     */
    protected $bannersModel;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param PostDataProcessor $dataProcessor
     * @param ModelBanners $bannersModel
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        PostDataProcessor $dataProcessor,
        ModelBanners $bannersModel,
        JsonFactory $jsonFactory
    )
    {
        parent::__construct($context);
        $this->dataProcessor = $dataProcessor;
        $this->bannersModel = $bannersModel;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Roweb_BannerSlider::banner_slider');
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @var \Magento\Framework\Controller\Result\Json $resultJson
     * @var \Magento\Cms\Model\Page $page
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $Id) {
            $banners = $this->bannersModel->load($Id);
            try {
                $Data = $this->filterPost($postItems[$Id]);
                $this->validatePost($Data, $banners, $error, $messages);
                $extendedPageData = $banners->getData();
                $this->setBannersData($banners, $extendedPageData, $Data);

                $this->bannersModel->save($banners);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithPageId($banners, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithPageId($banners, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithPageId(
                    $banners,
                    __('Something went wrong while saving the item.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * @param array $postData
     * @return array|mixed
     */
    protected function filterPost($postData = [])
    {
        return $postData;
    }

    /**
     * @param array $pageData
     * @param ModelBanners $page
     * @param $error
     * @param array $messages
     */
    protected function validatePost(array $pageData, ModelBanners $page, &$error, array &$messages)
    {
        if (!($this->dataProcessor->validate($pageData) && $this->dataProcessor->validateRequireEntry($pageData))) {
            $error = true;
            foreach ($this->messageManager->getMessages(true)->getItems() as $error) {
                $messages[] = $this->getErrorWithPageId($page, $error->getText());
            }
        }
    }

    /**
     * @param ModelBanners $page
     * @param $errorText
     * @return string
     */
    protected function getErrorWithPageId(ModelBanners $page, $errorText)
    {
        return '[Page ID: ' . $page->getId() . '] ' . $errorText;
    }

    /**
     * @param ModelBanners $page
     * @param array $extendedPageData
     * @param array $pageData
     * @return $this
     */
    public function setBannersData(ModelBanners $page, array $extendedPageData, array $pageData)
    {
        $page->setData(array_merge($page->getData(), $extendedPageData, $pageData));
        return $this;
    }
}
