<?php

namespace Roweb\BannerSlider\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template\Context;
use Roweb\BannerSlider\Helper\Data;
use Roweb\BannerSlider\Model\ResourceModel\Banners\CollectionFactory;

/**
 * Class Banners
 * @package Roweb\BannerSlider\Block
 */
class Banners extends \Magento\Framework\View\Element\Template
{

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var
     */
    protected $objectManager;

    /**
     * Banners constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param Data $dataHelper
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        Data $dataHelper
    )
    {
        $this->scopeConfig = $context->getScopeConfig();
        $this->collectionFactory = $collectionFactory;
        $this->storeManagerInterface = $context->getStoreManager();
        $this->dataHelper = $dataHelper;
        parent::__construct($context);
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getFrontBanners()
    {
        $collection = $this->collectionFactory->create()->addFieldToFilter('status', 1)->addFieldToFilter('store_views', array('in' => array(0, $this->storeManagerInterface->getStore()->getId())));
        if ($ids_list = $this->getBannerBlockArguments()) {
            $collection->addFilter('id', ['in' => $ids_list], 'public');
        }
        return $collection;
    }


    /**
     * @return array|false|string[]
     */
    public function getBannerBlockArguments()
    {
        $list = $this->getBannerList();
        $listArray = [];
        if ($list != '') {
            $listArray = explode(',', $list);
        }
        return $listArray;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaDirectoryUrl()
    {
        $media_dir = $this->storeManagerInterface->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $media_dir;
    }
}
