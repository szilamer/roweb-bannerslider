<?php

namespace Roweb\BannerSlider\Helper;

use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 * @package Roweb\BannerSlider\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     */
    const ENABLED = 'banner/general/enabled';

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getEnable()
    {
        return $this->scopeConfig->getValue(self::ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
