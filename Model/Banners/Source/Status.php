<?php

namespace Roweb\BannerSlider\Model\Banners\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 * @package Roweb\BannerSlider\Model\Banners\Source
 */
class Status implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = ['1' => 'Enable', '0' => 'Disable'];
        $options = [];
        foreach ($availableOptions as $key => $label) {
            $options[] = [
                'label' => $label,
                'value' => $key,
            ];
        }
        return $options;
    }
}
