<?php

namespace Roweb\BannerSlider\Model\ResourceModel\Banners;

use \Roweb\BannerSlider\Model\ResourceModel\AbstractCollection;

/**
 * Class Collection
 * @package Roweb\BannerSlider\Model\ResourceModel\Banners
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * @var
     */
    protected $_previewFlag;

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Roweb\BannerSlider\Model\Banners', 'Roweb\BannerSlider\Model\ResourceModel\Banners');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
