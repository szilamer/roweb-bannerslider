<?php

namespace Roweb\BannerSlider\Model\ResourceModel;

/**
 * Class Banners
 * @package Roweb\BannerSlider\Model\ResourceModel
 */
class Banners extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('rw_banners', 'id');
    }
}
