<?php

namespace Roweb\BannerSlider\Model;

/**
 * Class Banners
 * @package Roweb\BannerSlider\Model
 */
class Banners extends \Magento\Framework\Model\AbstractModel
{

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Roweb\BannerSlider\Model\ResourceModel\Banners');
    }

    /**
     * @return string[]
     */
    public function getAvailableStatuses()
    {
        return ['1' => 'Enable', '0' => 'Disable'];
    }
}
